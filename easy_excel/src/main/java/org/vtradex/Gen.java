package org.vtradex;

import org.vtradex.oms.BaseOrderDetailSnWrite;
import org.vtradex.oms.OmsStringCodeTrackWrite;
import org.vtradex.wms.WmsAsnDetailSnWrite;
import org.vtradex.wms.WmsPickTicketDetailSnWrite;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 请补充描述信息
 *
 * @author <a href="mailto:panpan.chen@vtradex.com">陈盼盼</a>
 * @version v1.0
 * @since 2023/12/30 22:02
 **/
public class Gen {
    public static void main(String[] args) throws IOException, InterruptedException {
        omsOrderDetailSn();
        TimeUnit.SECONDS.sleep(3);

        oms_string_code_track();
        TimeUnit.SECONDS.sleep(3);

        wms_pick_ticket_detail_sn();
        TimeUnit.SECONDS.sleep(3);

        wms_asn_detail_sn();
    }

    private static void omsOrderDetailSn() throws IOException {
        String date = new SimpleDateFormat("yyyy-MM-dd.HH:mm.ss").format(new Date());
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("out_sql/" + date + ".txt.sql", false));
        BaseOrderDetailSnWrite baseOrderDetailSnWrite = new BaseOrderDetailSnWrite(bufferedWriter);

        baseOrderDetailSnWrite.toSql(2, new File("input/base_order_detail_sn.xlsx"));
    }

    private static void oms_string_code_track() throws IOException {
        String date = new SimpleDateFormat("yyyy-MM-dd.HH:mm.ss").format(new Date());
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("out_sql/" + date + ".txt.sql", false));
        OmsStringCodeTrackWrite baseOrderDetailSnWrite = new OmsStringCodeTrackWrite(bufferedWriter);

        baseOrderDetailSnWrite.toSql(2, new File("input/oms_string_code_track.xlsx"));
    }

    private static void wms_pick_ticket_detail_sn() throws IOException {
        String date = new SimpleDateFormat("yyyy-MM-dd.HH:mm.ss").format(new Date());
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("out_sql/" + date + ".txt.sql", false));
        WmsPickTicketDetailSnWrite baseOrderDetailSnWrite = new WmsPickTicketDetailSnWrite(bufferedWriter);

        baseOrderDetailSnWrite.toSql(2, new File("input/wms_pick_ticket_detail_sn.xlsx"));
    }

    private static void wms_asn_detail_sn() throws IOException {
        String date = new SimpleDateFormat("yyyy-MM-dd.HH:mm.ss").format(new Date());
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("out_sql/" + date + ".txt.sql", false));
        WmsAsnDetailSnWrite baseOrderDetailSnWrite = new WmsAsnDetailSnWrite(bufferedWriter);

        baseOrderDetailSnWrite.toSql(2, new File("input/wms_asn_detail_sn.xlsx"));
    }
}
