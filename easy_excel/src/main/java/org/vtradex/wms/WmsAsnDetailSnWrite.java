package org.vtradex.wms;

import lombok.Data;
import org.vtradex.AbstractWrite;

import java.io.BufferedWriter;

/**
 * 请补充描述信息
 *
 * @author <a href="mailto:panpan.chen@vtradex.com">陈盼盼</a>
 * @version v1.0
 * @since 2023/12/31 21:45
 **/
public class WmsAsnDetailSnWrite extends AbstractWrite {
    public WmsAsnDetailSnWrite(BufferedWriter bufferedWriter) {
        super(bufferedWriter);
    }

    @Override
    protected String tableName() {
        return "wms_asn_detail_sn";
    }

    @Override
    protected Class entityCls() {
        return WmsAsnDetailSnWrite.WmsAsnDetailSn.class;
    }

    @Data
    public static class WmsAsnDetailSn{
        private String BE_DELETE;
        private String CREATED_TIME;
        private String CREATOR;
        private String CREATOR_UNIKEY;
        private String LAST_OPERATOR;
        private String LAST_OPERATOR_UNIKEY;
        private String UPDATE_TIME;

        private Long VERSION;
        private Long ASN_ID;

        private String CMEI;
        private String CUSTOMER_NUMBER;
        private String CUSTOMIZED_SN;
        private String IMEI;
        private String IMEI1;
        private String IMEI2;
        private String ITEM_ID;
        private String MAC;
        private String MAC2;
        private String MEID;
        private String PACKING2;
        private String PRODUCT_BARCODE;
        private String RELATED_NUMBER;
        private String SERIAL_NUMBER;
        private String SERIAL_NUMBER1;
        private String BE_SELF;
        private String GOODS_ATTRIBUTE;
        private String LPN;
        private String PALLET_NO;

        private Long ASN_DETAIL_ID;
    }
}
