package org.vtradex.oms;

import lombok.Data;
import org.vtradex.AbstractWrite;

import java.io.BufferedWriter;

/**
 * 请补充描述信息
 *
 * @author <a href="mailto:panpan.chen@vtradex.com">陈盼盼</a>
 * @version v1.0
 * @since 2023/12/30 10:12
 **/
public class BaseOrderDetailSnWrite extends AbstractWrite {

    public BaseOrderDetailSnWrite(BufferedWriter bufferedWriter) {
        super(bufferedWriter);
    }

    @Override
    protected String tableName() {
        return "base_order_detail_sn";
    }

    @Override
    protected Class entityCls() {
        return BaseOrderDetailSnWrite.BaseOrderDetailSn.class;
    }


    @Data
    public static class BaseOrderDetailSn {
        private String BE_DELETE;
        private String CREATED_TIME;
        private String CREATOR;
        private String CREATOR_UNIKEY;
        private String LAST_OPERATOR;
        private String LAST_OPERATOR_UNIKEY;
        private String UPDATE_TIME;

        private Long VERSION;

        private String CMEI;
        private String CUSTOMER_NUMBER;
        private String CUSTOMIZED_SN;
        private String GOODS_ID;
        private String IMEI;
        private String IMEI1;
        private String IMEI2;
        private String MAC;
        private String MAC2;
        private String MEID;


        private Long ORDER_ID;
        private String PACKING2;
        private String PRODUCT_BARCODE;
        private String RELATED_NUMBER;
        private String SERIAL_NUMBER;

        private String UDF01;
        private String UDF02;
        private String UDF03;
        private String UDF04;
        private String UDF05;
        private String UDF06;
        private String UDF07;
        private String UDF08;
        private String UDF09;
        private String UDF10;
        private String UDF11;
        private String UDF12;
        private String UDF13;
        private String UDF14;
        private String UDF15;
        private String UDF16;
        private String UDF17;
        private String UDF18;
        private String UDF19;
        private String UDF20;

        private String BE_SELF;
    }

}
