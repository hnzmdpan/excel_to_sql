package org.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 请补充描述信息
 *
 * @author <a href="mailto:panpan.chen@vtradex.com">陈盼盼</a>
 * @version v1.0
 * @since 2023/12/29 20:23
 **/
public class WriteTest {
    public static void main(String[] args) {
        String fileName = "xxx.xlsx";
        EasyExcel.write(fileName, DemoData.class).sheet("sheetName").doWrite(data());
    }

    private static List<DemoData> data() {
        ArrayList<DemoData> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            DemoData data = new DemoData();
            data.setSno(i);
            data.setSname("法外狂徒" + i);
            list.add(data);
        }
        return list;
    }

    @Data
    public static class DemoData {
        @ExcelProperty("学生编号")
        private int sno;
        @ExcelProperty("学生姓名")
        private String sname;
    }

}
