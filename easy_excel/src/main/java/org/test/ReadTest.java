package org.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 请补充描述信息
 *
 * @author <a href="mailto:panpan.chen@vtradex.com">陈盼盼</a>
 * @version v1.0
 * @since 2023/12/29 20:23
 **/
public class ReadTest {
    public static void main(String[] args) {
        String fileName = "01.xlsx";

//        EasyExcel.read(fileName, ReadData.class, new ExcelListener()).sheet().doRead();

        ExcelReader excelReader = EasyExcel.read(fileName, ReadData.class, new ExcelListener()).build();
        ReadSheet readSheet = EasyExcel.readSheet(0)
//                .headRowNumber(1)
                .build();

        excelReader.read(readSheet);
        excelReader.finish();

    }

    @Data
    public static class ReadData {
        @ExcelProperty(index = 0)
        private String sid;
        @ExcelProperty(index = 1)
        private String sname;
    }

    //创建读取excel监听器
    public static class ExcelListener extends AnalysisEventListener<ReadData> {

        //创建list集合封装最终的数据
        List<ReadData> list = new ArrayList<ReadData>();

        //一行一行去读取excle内容
        @Override
        public void invoke(ReadData user, AnalysisContext analysisContext) {
            System.out.println("***" + user);
        }

        //读取excel表头信息
        @Override
        public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
            System.out.println("表头信息：" + headMap);
        }

        //读取完成后执行
        @Override
        public void doAfterAllAnalysed(AnalysisContext analysisContext) {
            System.out.println(analysisContext);
        }
    }
}
