package org.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 请补充描述信息
 *
 * @author <a href="mailto:panpan.chen@vtradex.com">陈盼盼</a>
 * @version v1.0
 * @since 2023/12/29 20:23
 **/
public class WriteTest3 {
    public static void main(String[] args) {
        String fileName = "写入多行标题.xlsx";
        EasyExcel.write(fileName, DemoData.class)
                .head(manyHead())
//                .registerWriteHandler()
                .sheet("sheetName").doWrite(data());
    }

    // 多行标题
    private static List<List<String>> manyHead() {
        List<List<String>> head = new ArrayList<>();
        head.add(Arrays.asList("用户", "id"));
        head.add(Arrays.asList("用户", "用户名"));
        head.add(Arrays.asList("用户", "地址"));
        return head;
    }

    private static List<DemoData> data() {
        ArrayList<DemoData> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            DemoData data = new DemoData();
            data.setSno(i);
            data.setSname("法外狂徒" + i);
            list.add(data);
        }
        return list;
    }

    @Data
    public static class DemoData {
        @ExcelProperty("学生编号")
        private int sno;
        @ExcelProperty("学生姓名")
        private String sname;
    }

}
